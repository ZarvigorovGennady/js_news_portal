import React from 'react';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer__info">
                <p className="footer__title">Новостник</p>
                <p className="footer__text">Single Page Application</p>
            </div>           
            <span className="footer__lable">Дипломный проект</span>
            <div className="footer__info">
                <p className="footer__text footer__text_right">Made by</p>
                <p className="footer__title">Геннадий Зарвигоров</p>
            </div> 
        </footer>
    );
  }

export default Footer;
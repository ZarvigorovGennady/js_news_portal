import React from 'react';

import foto from '../../assets/img/foto.jpg'

const Contacts = () => {
    return ( 
        <div className="section">
            <div className="contacts">
                <div className="contacts-info">
                    <div className="contacts-info__data">
                        <h1 className="section__title section__title_contacts">+7 (929) 007 96 37</h1>
                        <div className="newsItem__block newsItem__block_contacts">
                            <p className="newsItem__title">Зарвигоров</p>
                            <p className="newsItem__title">Геннадий</p>
                        </div> 
                        <span className="contacts-info__addit">
                            webzarvigorov@yandex.ru
                        </span>
                    </div>
                    <div className="contacts-info__skills">
                        <p className="contacts-info__addit contacts-info__addit_opacity">JavaScript разработчик</p>     
                        <p className="contacts-info__addit">
                            ES5, ES6, 
                            <span className="contacts-info__addit contacts-info__addit_color"> React</span>
                        </p>     
                    </div>
                </div>
                <div className="contacts-foto">
                    <img className="contacts-foto__img" src={foto} alt="Developer"></img>
                </div>
            </div>
        </div>       
    );
  }

export default Contacts;
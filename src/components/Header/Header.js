import React from 'react';
import {NavLink} from 'react-router-dom';


const Header = () => {
    return (
        <header className="header">
            <nav className="nav">
                <NavLink exact to="/" className="nav__logo">Новостник</NavLink>
                <ul className="nav__menu">
                    <li>
                        <NavLink exact to="/" className="nav__item" activeClassName={"nav-active"}>Главная</NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/News" className="nav__item" activeClassName={"nav-active"}>Новости</NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/Contacts" className="nav__item" activeClassName={"nav-active"}>Контакты</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
  }

export default Header;
import React from 'react';
import {withRouter} from 'react-router-dom';


import convertDate from '../../functions/converteDate';

const NewsItem = props => {
    const news = props.news;
    const newsDate = new Date(news.publishedAt);  
          
    const day = convertDate(newsDate.getDate());
    const month = convertDate(newsDate.getMonth());

    return ( 
        <li onClick={()=>{props.history.push('/News/News' + props.newsIndex, props.news)}} className="newsItem">
            <div className="newsItem__block">
                <h2 className="newsItem__title">{news.title}</h2>
            </div>       
            <div className="newsItem__info">
                <span className="newsItem__source">{news.author}</span>
                <div className="newsItem__date">
                    <span className="newsItem__date__num">{day}</span>
                    <span className="newsItem__date__month">/ {month}</span>
                </div>
            </div>   
        </li> 
    );
  }

export default withRouter(NewsItem);



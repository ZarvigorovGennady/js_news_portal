import React from 'react';

import convertDate from '../../functions/converteDate';

const NewsDetail = props => { 
  const news = props.location.state;
  const newsDate = new Date(news.publishedAt);  
         
  const day = convertDate(newsDate.getDate());
  const month = convertDate(newsDate.getMonth());

  const lastWordTitle = news.title.split(" ").pop();
  const firstTitle = news.title.substr(0, news.title.length - lastWordTitle.length);
     
  return ( 
    <div className="section">
        <div className="singleNews">
            <div className="singleNews__block-left">
                <h1>
                    {firstTitle} 
                    <p style={{color: '#004FEC'}}>{lastWordTitle}</p> 
                </h1>
                <span className="newsItem__source single-span">
                    {news.author}  
                </span>
                <div className="newsItem__date newsItem__date_single">
                    <span className="newsItem__date__num">{day}</span>
                    <span className="newsItem__date__month">/ {month}</span>
                </div>
            </div>
            <div className="singleNews__block-right">
                <img src={news.urlToImage} alt="News">
                </img>
                <p>
                  {news.content}
                </p>
            </div>
        </div>  
    </div>   
  ); 
}

export default NewsDetail;
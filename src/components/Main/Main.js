import React from 'react';
import {NavLink} from 'react-router-dom';

import NewsList from '../NewsList/NewsList';

const Main = props => {
    const styleObj = {
        fontSize: "20px",
        color : "#004FEC",
    }
    return (
        <div className="main">
            <div className="main-offer section">
                <h1 className="section__title">Всегда <br /> свежие <span style={{color: '#004FEC'}}>новости</span></h1>
                <NewsList newsData={props.newsData} lengthList={6}/>
                <div className="hover-link">
                    <NavLink exact to="/News" style={styleObj}>Быть в курсе событий</NavLink>
                </div>   
            </div>
        </div>
    );
  }

export default Main;




import React from 'react';

import NewsList from '../NewsList/NewsList';

const News = props => {
    return ( 
        <div className="section">
            <h1 className="section__title">Быть <br/> в курсе <span style={{color: '#004FEC'}}>событий</span></h1>
            <NewsList newsData={props.newsData} lengthList={9}/>
        </div> 
    );
  }

export default News;
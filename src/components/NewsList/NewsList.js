import React from 'react';

import NewsItem from '../NewsItem/NewsItem';

const NewsList = props => { 
// Sorting the length of the news list
    const news = props.newsData.articles;
    const newsSorting = news.filter((news, index) =>{
        return index < props.lengthList;
    });
    
// Rendering a list of news
    const finalNews = newsSorting.map((item,index) =>{              
        return (
            <NewsItem
                key={index}
                news={item}
                newsIndex={index + 1}
            />
        )
    })
    return <ul className="newsList">{finalNews}</ul>        
  }

export default NewsList;
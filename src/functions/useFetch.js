import {useState, useEffect} from 'react';
import axios from 'axios';

const useFetch = () => {
    const [data, updateData] = useState(null);
    const requestUrl = 'https://newsapi.org/v2/top-headlines?' +
        'sources=bbc-news&' +
        'apiKey=6b15e83dc66643d4909fe0b274729c82';
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(requestUrl);
            updateData(response.data)
        }
        fetchData();
    },[requestUrl])
    return data;
}

export default useFetch;
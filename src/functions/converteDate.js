
const convertDate = (date) => {
    if(date < 10){
        const dateConvert = '0' + date.toString();
        return dateConvert;   
    } else {
        return date; 
    }  
}  

export default convertDate;
import React from 'react';
import {Route} from 'react-router-dom';

import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Footer from './components/Footer/Footer';
import News from './components/News/News';
import Contacts from './components/Contacts/Contacts';
import NewsDetail from './components/NewsDetail/NewsDetail';

import useFetch from './functions/useFetch';

import './App.css';

function App() {  
  const newsData = useFetch();
  
  const showPages = () => {
    return (
      <div className="news">
        <Header/>
        <Route
          path="/"
          exact
          render={()=><Main newsData={newsData}/>}
        />
        <Route
          path="/News"
          exact
          render={()=><News newsData={newsData}/>}
        />
        <Route
          path="/Contacts"
          exact
          component={Contacts}
        />
        <Route
          path="/News/News:name"
          exact
          component={NewsDetail}
        />
        <Footer/>
      </div>
    );
  }
  return <div className="App">{newsData ? showPages() : null} </div>
}

export default App;
